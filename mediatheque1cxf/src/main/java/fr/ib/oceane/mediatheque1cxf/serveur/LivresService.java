package fr.ib.oceane.mediatheque1cxf.serveur;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

@WebService
public class LivresService implements ILivresService {
	
public String getInfos() {
	return "L'entrée de la bibliotheque se trouve ici";
}
public boolean estEnpruntable(int id) {
	if (id<1)
		throw new IllegalArgumentException("id doit etre 1 ou plus");
	
	return false;
}
public Date getRetour(int id) {
	//renvoyer toujours : aujourd'hui + 10
	//return LocalDate.now().plusDays(10);
	//LocalDate ne marche pas donc on utilise une ancienne methode
	return  Date.from(LocalDate.now().plusDays(10).atStartOfDay(ZoneId.systemDefault()).toInstant());
}
public Livre getLivreDuMois() {
	Livre l = new Livre("La plus secrete ...","M. M. Sarr", 2021);
	DataSource dataSource = new FileDataSource("la-plus-secrete.jpg"); // pour inserer une image
	DataHandler dataHandler = new DataHandler(dataSource);
	l.setImage(dataHandler);
	return l;
}

public Livre[] getLivresDeLannee() {
	Livre[] livres = new Livre[12]; //tableau de 12 elements
	for(int i=0; i<12 ; i++) { //on remplit le tableau avec le livre du mois 
		livres[i] =getLivreDuMois();
	}
	return livres;
}
}
