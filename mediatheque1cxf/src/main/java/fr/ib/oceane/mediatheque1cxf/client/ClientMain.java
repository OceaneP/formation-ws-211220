package fr.ib.oceane.mediatheque1cxf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.oceane.mediatheque1cxf.serveur.ILivresService;

public class ClientMain {

	public static void main(String[] args) {
		System.out.println("Client livres : ");
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres"); //lien reseau (ou aller)
		factory.setServiceClass(ILivresService.class);//lien avec les fonction de l'interface (que chercher)
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add(new LoggingInInterceptor());
		ILivresService livresServices =factory.create(ILivresService.class);
		System.out.println(livresServices.getInfos());//afficher fonction getInfos de l'interface
		System.out.println("Livre 4 enpruntable : " +livresServices.estEnpruntable(4));
		try {
		System.out.println("Livre -3 enpruntable : " +livresServices.estEnpruntable(-3));
		}catch (Exception ex) {
			System.out.println("Exception " + ex.getMessage());
		}
		System.out.println("Retour du livre 4 : " + livresServices.getRetour(4));
		System.out.println("Livre du mois : " +livresServices.getLivreDuMois() );
		System.out.println("Livre du mois de mars : " +livresServices.getLivresDeLannee()[2].getTitre() );
	}
	}


