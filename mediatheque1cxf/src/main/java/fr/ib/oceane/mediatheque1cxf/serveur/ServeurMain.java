/**
 * 
 */
package fr.ib.oceane.mediatheque1cxf.serveur;

import javax.xml.ws.Endpoint;

/**
 * @author ib
 *
 */
public class ServeurMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Demarrage du serveur");
		
		Endpoint epLivres = Endpoint.publish(
				"http://localhost:9001/livres",
				new LivresService());
	//epLivres.stop(); ne pas garder car fait bugger le truc

	}

}
