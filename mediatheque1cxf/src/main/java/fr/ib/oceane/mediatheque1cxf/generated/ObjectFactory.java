
package fr.ib.oceane.mediatheque1cxf.generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.ib.oceane.mediatheque1cxf.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EstEnpruntable_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "estEnpruntable");
    private final static QName _EstEnpruntableResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "estEnpruntableResponse");
    private final static QName _GetInfos_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "getInfos");
    private final static QName _GetInfosResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "getInfosResponse");
    private final static QName _GetLivreDuMois_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "getLivreDuMois");
    private final static QName _GetLivreDuMoisResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "getLivreDuMoisResponse");
    private final static QName _GetLivresDeLannee_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "getLivresDeLannee");
    private final static QName _GetLivresDeLanneeResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "getLivresDeLanneeResponse");
    private final static QName _GetRetour_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "getRetour");
    private final static QName _GetRetourResponse_QNAME = new QName("http://serveur.mediatheque1cxf.oceane.ib.fr/", "getRetourResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.ib.oceane.mediatheque1cxf.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EstEnpruntable }
     * 
     */
    public EstEnpruntable createEstEnpruntable() {
        return new EstEnpruntable();
    }

    /**
     * Create an instance of {@link EstEnpruntableResponse }
     * 
     */
    public EstEnpruntableResponse createEstEnpruntableResponse() {
        return new EstEnpruntableResponse();
    }

    /**
     * Create an instance of {@link GetInfos }
     * 
     */
    public GetInfos createGetInfos() {
        return new GetInfos();
    }

    /**
     * Create an instance of {@link GetInfosResponse }
     * 
     */
    public GetInfosResponse createGetInfosResponse() {
        return new GetInfosResponse();
    }

    /**
     * Create an instance of {@link GetLivreDuMois }
     * 
     */
    public GetLivreDuMois createGetLivreDuMois() {
        return new GetLivreDuMois();
    }

    /**
     * Create an instance of {@link GetLivreDuMoisResponse }
     * 
     */
    public GetLivreDuMoisResponse createGetLivreDuMoisResponse() {
        return new GetLivreDuMoisResponse();
    }

    /**
     * Create an instance of {@link GetLivresDeLannee }
     * 
     */
    public GetLivresDeLannee createGetLivresDeLannee() {
        return new GetLivresDeLannee();
    }

    /**
     * Create an instance of {@link GetLivresDeLanneeResponse }
     * 
     */
    public GetLivresDeLanneeResponse createGetLivresDeLanneeResponse() {
        return new GetLivresDeLanneeResponse();
    }

    /**
     * Create an instance of {@link GetRetour }
     * 
     */
    public GetRetour createGetRetour() {
        return new GetRetour();
    }

    /**
     * Create an instance of {@link GetRetourResponse }
     * 
     */
    public GetRetourResponse createGetRetourResponse() {
        return new GetRetourResponse();
    }

    /**
     * Create an instance of {@link Livre }
     * 
     */
    public Livre createLivre() {
        return new Livre();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstEnpruntable }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EstEnpruntable }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "estEnpruntable")
    public JAXBElement<EstEnpruntable> createEstEnpruntable(EstEnpruntable value) {
        return new JAXBElement<EstEnpruntable>(_EstEnpruntable_QNAME, EstEnpruntable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstEnpruntableResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EstEnpruntableResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "estEnpruntableResponse")
    public JAXBElement<EstEnpruntableResponse> createEstEnpruntableResponse(EstEnpruntableResponse value) {
        return new JAXBElement<EstEnpruntableResponse>(_EstEnpruntableResponse_QNAME, EstEnpruntableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInfos }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetInfos }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "getInfos")
    public JAXBElement<GetInfos> createGetInfos(GetInfos value) {
        return new JAXBElement<GetInfos>(_GetInfos_QNAME, GetInfos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInfosResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetInfosResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "getInfosResponse")
    public JAXBElement<GetInfosResponse> createGetInfosResponse(GetInfosResponse value) {
        return new JAXBElement<GetInfosResponse>(_GetInfosResponse_QNAME, GetInfosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLivreDuMois }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLivreDuMois }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "getLivreDuMois")
    public JAXBElement<GetLivreDuMois> createGetLivreDuMois(GetLivreDuMois value) {
        return new JAXBElement<GetLivreDuMois>(_GetLivreDuMois_QNAME, GetLivreDuMois.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLivreDuMoisResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLivreDuMoisResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "getLivreDuMoisResponse")
    public JAXBElement<GetLivreDuMoisResponse> createGetLivreDuMoisResponse(GetLivreDuMoisResponse value) {
        return new JAXBElement<GetLivreDuMoisResponse>(_GetLivreDuMoisResponse_QNAME, GetLivreDuMoisResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLivresDeLannee }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLivresDeLannee }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "getLivresDeLannee")
    public JAXBElement<GetLivresDeLannee> createGetLivresDeLannee(GetLivresDeLannee value) {
        return new JAXBElement<GetLivresDeLannee>(_GetLivresDeLannee_QNAME, GetLivresDeLannee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLivresDeLanneeResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLivresDeLanneeResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "getLivresDeLanneeResponse")
    public JAXBElement<GetLivresDeLanneeResponse> createGetLivresDeLanneeResponse(GetLivresDeLanneeResponse value) {
        return new JAXBElement<GetLivresDeLanneeResponse>(_GetLivresDeLanneeResponse_QNAME, GetLivresDeLanneeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRetour }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetRetour }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "getRetour")
    public JAXBElement<GetRetour> createGetRetour(GetRetour value) {
        return new JAXBElement<GetRetour>(_GetRetour_QNAME, GetRetour.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRetourResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetRetourResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://serveur.mediatheque1cxf.oceane.ib.fr/", name = "getRetourResponse")
    public JAXBElement<GetRetourResponse> createGetRetourResponse(GetRetourResponse value) {
        return new JAXBElement<GetRetourResponse>(_GetRetourResponse_QNAME, GetRetourResponse.class, null, value);
    }

}
