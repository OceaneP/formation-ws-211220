package fr.ib.oceane.mediatheque4web;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalTime;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ib.oceane.mediatheque3ejb.Dvd;
import fr.ib.oceane.mediatheque3ejb.IDvdDAO;
import fr.ib.oceane.mediatheque3ejb.IDvdtheque;


@WebServlet("/dvds")//remplace l'annotation dans web.xml
public class DvdsServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private IDvdtheque dvdtheque;
	private IDvdDAO dvdDAO;
	
@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		
		Writer out = resp.getWriter();
		out.write("<!DOCTYPE html><html><body>");
		out.write("<h1>DVDtheque</h1>");
		out.write("<p>"+dvdtheque.getInfos()+"</p>");
		out.write("<p> Ouvert à 9h? "+dvdtheque.ouvertA(LocalTime.of(9, 0))+"</p>");
		out.write("<p> Quand ? "+dvdtheque.getDerniereInterrogation()+"</p>");
		out.write("<table>");
		
		
		out.write("<tr><th>Titre</th><th>Annee</th></tr>");
		for (Dvd dvd : dvdDAO.lireTous()) {
			
		
		out.write("<tr><td>"+dvd.getTitre()+"</td>" +
				"<td>"+dvd.getAnnee()+"</td></tr>");
		}
		out.write("</table>");
		out.write("</body></html>");
		
	
		out.close();
	}

@EJB(lookup="ejb:/Mediatheque3Ejb/Dvds!fr.ib.oceane.mediatheque3ejb.IDvdtheque")
public void setDvdtheque(IDvdtheque dvdtheque) {
	this.dvdtheque = dvdtheque;
}
@EJB(lookup="ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.oceane.mediatheque3ejb.IDvdDAO")
public void setDvdDAO(IDvdDAO dvdDAO) {
	this.dvdDAO =  dvdDAO;
}

}
