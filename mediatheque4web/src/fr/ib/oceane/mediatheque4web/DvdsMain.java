package fr.ib.oceane.mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.ib.oceane.mediatheque3ejb.Dvd;
import fr.ib.oceane.mediatheque3ejb.IDvdDAO;
import fr.ib.oceane.mediatheque3ejb.IDvdtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("Dvd : client lourd");
		try {
Context context = new InitialContext();
IDvdtheque dvdtheque =(IDvdtheque) context.lookup("ejb:/Mediatheque3Ejb/Dvds!fr.ib.oceane.mediatheque3ejb.IDvdtheque");
System.out.println("Informations : " + dvdtheque.getInfos());
System.out.println("Ouvert à  17h30? : " + dvdtheque.ouvertA(LocalTime.of(17, 30)));
IDvdDAO dvdDAO =(IDvdDAO) context.lookup("ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.oceane.mediatheque3ejb.IDvdDAO");//penser a copier dans le Mediatheque4 l'interface ainsi que la classe DVD de mediatheque3 et a mettre dans les classes DVD implements Serializable
dvdDAO.ajouter(new Dvd ("Maman j'ai raté l'avion", 1997));
dvdDAO.ajouter(new Dvd ("Super Noel", 2004));
dvdDAO.ajouter(new Dvd ("Hello World", 2021));
System.out.println("Il y a maintenant  : " + dvdDAO.getNombre());
context.close();
		} catch (Exception ex ) {
			System.err.println(ex);
			ex.printStackTrace();
		}
		}

}
