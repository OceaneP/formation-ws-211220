package fr.ib.oceane.mediatheque3ejb;

import java.util.List;

import javax.ejb.Remote;

@Remote // ou local si l'on veut qu'il ne soit pas accessible partout
public interface IDvdDAO {
public int getNombre();
public void ajouter(Dvd dvd);
public Dvd lire(int id);
public List<Dvd> lireTous();
}
