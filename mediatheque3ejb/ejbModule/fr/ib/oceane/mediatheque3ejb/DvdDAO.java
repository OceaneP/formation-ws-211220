package fr.ib.oceane.mediatheque3ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name="DvdDAO",description = "Stockage JPA des DVD")
public class DvdDAO implements IDvdDAO{
	private EntityManager em; //idem hibernate juste les nom qui change
	
	public int getNombre() {
		return em.createQuery("select count(*) from Dvd", Long.class).getSingleResult().intValue();//intValue converti en entier 
	}

	@Override
	public void ajouter(Dvd dvd) {
		em.persist(dvd);
		
	}

	@Override
	public Dvd lire(int id) {
		
		return em.find(Dvd.class, id);
	}

	@Override
	public List<Dvd> lireTous() {
		
		return em.createQuery("from Dvd order by annee", Dvd.class).getResultList();
	}
@PersistenceContext(unitName="DvdPU")
	public void setEm(EntityManager em) {
		this.em = em;
	}
	
	
}
