package fr.ib.oceane.mediatheque3ejb;


import java.time.LocalTime;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

@Stateful(name="Dvds", description="Operation pour la DvdTheque") //Stateless : ce qu'il y a deans les attribut n'est pas interessant sinon on utilise Statefull
public class Dvdtheque implements IDvdtheque{
	private LocalTime derniereInterrogation;
	private IDvdDAO dvdDAO;
public String getInfos() {
	return "Nouvelle DVDtheque, ouverte de 10h à 18h \n" +
			"Il y a " + dvdDAO.getNombre()+" DVD";

}
public boolean ouvertA(LocalTime t) {
	derniereInterrogation =t;
	return t.isAfter(LocalTime.of(10, 0))&& t.isBefore(LocalTime.of(18, 0));
}

public LocalTime getDerniereInterrogation() {
	return derniereInterrogation;
}

//@EJB(lookup="ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.oceane.mediatheque3ejb.IDvdDAO")
@EJB(name="DvdDAO")// autre maniere de faire
public void setDvdDAO(IDvdDAO dvdDAO) {
	this.dvdDAO = dvdDAO;
}



}
