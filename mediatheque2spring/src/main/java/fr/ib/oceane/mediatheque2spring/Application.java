package fr.ib.oceane.mediatheque2spring;


	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
	import org.springframework.boot.SpringApplication;
	import org.springframework.boot.autoconfigure.SpringBootApplication;


	/**
	 * @author ib
	 *
	 */
	@SpringBootApplication
	public class Application {

		/**
		 * @param args
		 */
		
		private static final Logger logger = LoggerFactory.getLogger(Application.class);
		public static void main(String[] args) {
			SpringApplication.run(Application.class, args);
			logger.info("Application correctement demarrée");
			//en vrac : throw new RuntimeException ("Catastrophe !");

		}
	
		
	}
	

