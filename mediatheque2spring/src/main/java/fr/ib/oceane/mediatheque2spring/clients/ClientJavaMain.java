package fr.ib.oceane.mediatheque2spring.clients;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;



public class ClientJavaMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java");
			URL url = new URL("http://localhost:9002/health"); //objet URL 
			URLConnection conn = url.openConnection();//creation d'une connection a partir de cet url
			InputStream is = conn.getInputStream();//flux d'entree binaiare
			BufferedReader br = new BufferedReader(new InputStreamReader(is));//pour convertir le flux binaire en flux texte 
			String reponse = br.readLine();//pour lire le flux texte ligne par ligne
			System.out.println("Health :" + reponse);
			br.close();
		} catch (Exception ex) {
			System.err.println("Erreur :" + ex);
		}

	}

}
