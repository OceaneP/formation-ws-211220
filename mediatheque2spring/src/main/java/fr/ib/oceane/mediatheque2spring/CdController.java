package fr.ib.oceane.mediatheque2spring;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CdController {
private CdService cdService;


@Autowired
public void setCdService(CdService cdService) {
	this.cdService = cdService;
}

@RequestMapping(path="/cd/nombre",produces=MediaType.APPLICATION_XML_VALUE, method=RequestMethod.GET)//pour changer le format par defaut (json) en xml
public int getNombre() {
	return cdService.getNombreDeCd();//renvoie un fichier json par defaut
}
@RequestMapping(path="/cd", method=RequestMethod.POST)
public void ajoute(@RequestBody Cd cd) { //@RequestBody indique que le cd ici est a l'interieur du corps de la requeque fabrique par le client 
	cdService.ajouteCd(cd);
}

@RequestMapping(path="/cd",method=RequestMethod.GET)
public List<Cd> getTous(){
	return 	cdService.getCds();
}
//num est composé de 1 ou plusieurs chiffres : \\d+
@RequestMapping(path="/cd/{num:\\d+}",method=RequestMethod.GET)
public Cd getUn(@PathVariable("num") int n) { // l'argument de la fonction (n) va le mettre dans le num
	return cdService.getCd(n);
}

//pour changer le titre du cd
@RequestMapping(path="/cd/{num:\\d+}/titre",method=RequestMethod.PUT)
public void modifieTitre(@PathVariable("num") int n,@RequestBody String titre) { // l'argument de la fonction (n) va le mettre dans le num
	 cdService.modifieTitre(n, titre);}

@RequestMapping(path="/json/cd",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
public List<Cd> getTousJson(){
	 return cdService.getCds();}
}
