package fr.ib.oceane.mediatheque2spring.clients;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.oceane.mediatheque2spring.Cd;

public class ClientSpringMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java Spring");
			RestTemplate rt = new RestTemplate();
			String health = rt.getForObject("http://localhost:9002/health", String.class);
			System.out.println("Health : " + health);
			
			Integer nbCd = rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);
			System.out.println("Il y a " + nbCd + " cd à la mediatheque");
			Cd cd1 = new Cd ("Abbey Road", "The Beatles", 1966);
			rt.postForObject("http://localhost:9002/cd", cd1, Void.class); // Void.class permet de dire qu'il n'y  a pas resultat
			
			
			
			//pour afficher tous les cd
			// ne fonctionne pas  :
			//List<Cd> cdTous = rt.getForObject("http://localhost:9002/cd",List<Cd>.class);
			
			// 1ere solution fonctionne :
			//ParameterizedTypeReference<List<Cd>> ref = new ParameterizedTypeReference<List<Cd>>() {};
			//ResponseEntity<List<Cd>> cdEntity = rt.exchange("http://localhost:9002/cd",HttpMethod.GET,null,ref);
			//List<Cd> cdTous = cdEntity.getBody();
			
			// 2eme solution avec des tableaux
			Cd[] cdTous =rt.getForObject("http://localhost:9002/cd",Cd[].class);
			System.out.println("Tous les CD : ");
			for (Cd cd:cdTous) {
			      System.out.println("- " + cd);
			}
			
			//parametre de requete : "http://localhost:9002/cd?no=0"
			Cd cd0 =rt.getForObject("http://localhost:9002/cd/0",Cd.class);
			System.out.println("Premier CD : "+ cd0);
		
			//pour modifier le titre du cd 0
			rt.put("http://localhost:9002/cd/0/titre", "Help", String.class);
			
		} catch (Exception ex) {
			System.err.println("Erreur :" + ex);
		}

	
	}
}

